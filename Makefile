.PHONY: all build clean lint run start-local stop-local test

all: test run

# Build

audit:
	yarn run audit

build:
	yarn build

clean:
	rm -rf dist/*

lint:
	yarn lint:hbs
	yarn lint:js

run:
	yarn start

test:
	yarn test

update:
	yarn update

# Docker

start-local:
	docker-compose up --build -d

stop-local:
	docker-compose down
