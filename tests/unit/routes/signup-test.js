import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | signup', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:signup');
    expect(route).to.be.ok;
  });
});
