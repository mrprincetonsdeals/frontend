import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | logout', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:logout');
    expect(route).to.be.ok;
  });
});
