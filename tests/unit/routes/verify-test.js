import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | verify', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:verify');
    expect(route).to.be.ok;
  });
});
