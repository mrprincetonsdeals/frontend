import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | companies/show', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:companies/show');
    expect(route).to.be.ok;
  });
});
