import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | friends/show', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:friends/show');
    expect(route).to.be.ok;
  });
});
