import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | friends/index', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:friends/index');
    expect(route).to.be.ok;
  });
});
