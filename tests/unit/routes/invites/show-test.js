import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | invites/show', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:invites/show');
    expect(route).to.be.ok;
  });
});
