import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | offers/show', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:offers/show');
    expect(route).to.be.ok;
  });
});
