import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | privacy', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:privacy');
    expect(route).to.be.ok;
  });
});
