import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | friend-list-cell', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<FriendListCell />`);

    expect(this.element).to.exist;
  });
});
