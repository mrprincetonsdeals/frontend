import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | code-create', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<CodeCreate />`);

    expect(this.element.querySelector('label')).to.exist;
    expect(this.element.querySelector('input')).to.exist;
    expect(this.element.querySelector('button')).to.exist;
  });
});
