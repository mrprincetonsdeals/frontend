import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | deal-list-cell', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<DealListCell />`);

    expect(this.element).to.exist;
  });
});
