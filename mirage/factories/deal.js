import { Factory } from 'ember-cli-mirage';
import faker from 'faker';

export default Factory.extend({
  linkToDeal: faker.internet.url,
  description: faker.lorem.sentences,
  userId: faker.random.number,

  // company: DS.belongsTo('company'),
});
