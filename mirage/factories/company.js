import { Factory } from 'ember-cli-mirage';
import faker from 'faker';

export default Factory.extend({
  name: faker.company.companyName,
  description: faker.company.catchPhrase,
  deal: faker.lorem.sentence,
});
