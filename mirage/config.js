export default function() {
  this.passthrough('/write-coverage');

  this.namespace = '/api/v1';
  this.timing = 400;

  this.get('/companies');
  this.get('/companies/:id');
  this.get('/deals');
  this.delete('/deals/:id');
  this.get('/friends');
  this.get('/friends/:id');
  this.get('/offers/:id');
}
