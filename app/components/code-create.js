import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class CodeCreateComponent extends Component {
  @service store;

  @tracked code = this.store.createRecord('code');
  @tracked errors;

  willDestroy() {
    super.willDestroy(...arguments);

    const code = this.code;

    if (!code.isDestroyed && !code.isDestroying) {
      code.destroyRecord();
    }
  }

  @action
  saveCode() {
    if (!this.code.url) {
      this.errors = 'errors.blankCode';

      return;
    }

    this.errors = null;
    this.code.save().then(() => {
      this.code = this.store.createRecord('code');
      this.args.refresh();
    });
  }
}
