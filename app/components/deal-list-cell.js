import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';

export default class DealListCellComponent extends Component {
  @service store;

  @action
  showCode(deal) {
    const codeId = deal.codeId;

    this.store.findRecord('code', codeId).then((code) => {
      deal.code = code;
    });
  }

  @action
  delete(deal) {
    deal.destroyRecord();
  }
}
