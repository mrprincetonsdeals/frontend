import EmberRouter from '@ember/routing/router';
import config from 'referral-codes-frontend/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function() {
  this.route('index', { path: '/' });
  this.route('profile');
  this.route('companies', function() {
    this.route('show', { path: ':company_id' });
  });
  this.route('friends', function() {
    this.route('show', { path: ':friend_id' });
  });
  this.route('offers', function() {
    this.route('show', { path: ':offer_id' });
  });
  this.route('invites', function() {
    this.route('show', { path: ':invite_code' });
  });

  this.route('login');
  this.route('logout');
  this.route('signup');
  this.route('verify');

  this.route('privacy');
});
