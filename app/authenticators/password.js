import Base from 'ember-simple-auth/authenticators/base';
import { Promise } from 'rsvp';

export default Base.extend({
  restore(data) {
    return Promise.resolve(data);
  },

  authenticate(data) {
    return Promise.resolve(data);
  },

  invalidate(data) {
    return Promise.resolve(data);
  },
});
