import Controller from '@ember/controller';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class PagedController extends Controller {
  queryParams = ['page'];

  @tracked page = 0;
  @tracked total;
  @tracked limit;

  get firstPage() {
    let page = this.page;

    return page === 0;
  }

  get lastPage() {
    let total = this.total,
      page = this.page,
      limit = this.limit;

    return (total - page * limit) <= limit;
  }

  @action prev() {
    this.page = this.page - 1;
  }

  @action next() {
    this.page = this.page + 1;
  }
}
