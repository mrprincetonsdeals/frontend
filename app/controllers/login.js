import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import fetch from 'fetch';

export default class LoginController extends Controller {
  queryParams = ['friendCode'];

  @tracked friendCode;

  @service session;

  @tracked email;
  @tracked password;
  @tracked errors;

  validateForm() {
    let errors = [];

    if (!this.email) {
      errors.push('errors.blankEmail');
    }

    if (!this.password) {
      errors.push('errors.blankPassword');
    }

    return errors;
  }

  @action
  async authenticate() {
    this.errors = null;

    const errors = this.validateForm();
    if (errors.length) {
      this.errors = errors;

      return;
    }

    const unknownError = 'errors.unknown',
      url = '/api/v1/login',
      data = {
        email: this.email,
        password: this.password,
      };

    if (this.friendCode) {
      data.friendCode = this.friendCode;
    }

    try {
      const requestOptions = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      };

      const response = await fetch(url, requestOptions)

      if (response.ok) {
        this.session.authenticate('authenticator:password', {});

        let previousTransition = this.previousTransition;
        if (previousTransition) {
          this.previousTransition = null;
          previousTransition.retry();
        } else {
          this.transitionToRoute('profile');
        }
      } else {
        const json = await response.json()

        throw json;
      }
    } catch(e) {
      if (e && e.errors) {
        this.errors = e.errors;
      } else {
        this.errors = unknownError;
      }
    }
  }
}
