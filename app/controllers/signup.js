import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import fetch from 'fetch';

export default class SignupController extends Controller {
  queryParams = ['friendCode'];

  @tracked friendCode;

  @service session;

  @tracked email;
  @tracked username;
  @tracked password;
  @tracked verifyPassword;
  @tracked submitted;
  @tracked errors;

  validateForm() {
    let errors = [];

    if (!this.email) {
      errors.push('errors.blankEmail');
    }

    if (!this.username) {
      errors.push('errors.blankUsername');
    }

    if (!this.password && !this.verifyPassword) {
      errors.push('errors.blankPassword');
    }

    if (this.password !== this.verifyPassword) {
      errors.push('errors.passwordMismatch');
    }

    return errors;
  }

  @action
  async register() {
    this.errors = null;

    const errors = this.validateForm();
    if (errors.length) {
      this.errors = errors;

      return;
    }

    const unknownError = 'errors.unknown',
      url = '/api/v1/signup',
      data = {
        email: this.email,
        username: this.username,
        password: this.password,
        verifyPassword: this.verifyPassword,
      };

    if (this.friendCode) {
      data.friendCode = this.friendCode;
    }

    try {
      const requestOptions = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(data),
        };

      const response = await fetch(url, requestOptions);

      if (response.ok) {
        this.submitted = true;
      } else {
        const json = await response.json();

        throw json;
      }
    } catch(e) {
      if (e && e.errors) {
        this.errors = e.errors;
      } else {
        this.errors = unknownError;
      }
    }
  }
}
