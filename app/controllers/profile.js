import Controller from '@ember/controller';
import { action } from '@ember/object';

export default class ProfileController extends Controller {
  get deals() {
    return this.model.reduce((deals, deal) => {
      if (!deal.isNew) {
        deals = deals || {};

        if (deal.belongsTo('company').id()) {
          deals.verified = deals.verified || [];
          deals.verified.push(deal);
        } else {
          deals.unverified = deals.unverified || [];
          deals.unverified.push(deal);
        }
      }

      return deals;
    });
  }

  @action
  refresh() {
    this.refreshRoute();
  }
}
