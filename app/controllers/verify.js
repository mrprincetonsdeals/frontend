import Controller from '@ember/controller';
import { tracked } from '@glimmer/tracking';

export default class VerifyController extends Controller {
  queryParams = ['token'];
  @tracked token = null;
}
