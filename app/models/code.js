import Model, { attr, belongsTo } from '@ember-data/model';

export default class CodeModel extends Model {
  @attr('string') url;
  @belongsTo('deal') deal;
}
