import Model, { attr, hasMany } from '@ember-data/model';

export default class FriendModel extends Model {
  @attr('string') name;
  @attr('number', { defaultValue: 0 }) dealCount;

  @hasMany('deal') deals;

  get dealCount() {
    let count = this.deals.length || 0;

    return count;
  }
}
