import Model, { attr, belongsTo, hasMany } from '@ember-data/model';

export default class OfferModel extends Model {
  @attr('string') description;

  @belongsTo('company') company;
  @hasMany('deal') deals;
}
