import Model, { attr, hasMany } from '@ember-data/model';

export default class CompanyModel extends Model {
  @attr('string') name;
  @attr('string') description;
  @attr('string') logoURL;

  @hasMany('offer') offers;
  @hasMany('deal') deals;

  get offersCount() {
    let count = this.offers.length || 0;

    return count;
  }
}
