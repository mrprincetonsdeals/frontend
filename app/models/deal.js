import Model, { attr, belongsTo } from '@ember-data/model';

export default class DealModel extends Model {
  @attr('string') codeId;

  @belongsTo('code') code;
  @belongsTo('company') company;
  @belongsTo('offer') offer;
  @belongsTo('friend') friend;
}
