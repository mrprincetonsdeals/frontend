import Route from '@ember/routing/route';
import fetch from 'fetch';

export default class VerifyRoute extends Route {
  queryParams = {
    token: {
      refreshModel: true,
    },
  };

  async model(params) {
    let errors = null;

    const token = params.token,
      missingTokenError = 'errors.missingToken',
      unknownError = 'errors.unknown',
      url = '/api/v1/verify',
      data = {
        verifyToken: token,
      };

    if (token) {
      try {
        const requestOptions = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(data),
        };

        const response = await fetch(url, requestOptions);

        if (response.ok) {
          this.transitionTo('login');
        } else {
          const json = await response.json()

          throw json;
        }
      } catch(e) {
        if (e && e.errors) {
          errors = e.errors;
        } else {
          errors = unknownError;
        }
      }
    } else {
      errors = missingTokenError;
    }

    return errors;
  }
}
