import Route from '@ember/routing/route';
import fetch from 'fetch';
import { inject as service } from '@ember/service';

export default class InvitesShowRoute extends Route {
  @service session;

  async model(params) {
    const unknownError = 'errors.unknown',
      invalidCodeError = 'errors.invalidInviteCode',
      url = `/api/v1/invites/${params.invite_code}`,
      friendCode = params.invite_code;

    if (friendCode) {
      let appController = this.controllerFor('application');
      appController.set('friendCode', friendCode);
    }

    try {
      if (!this.session.isAuthenticated) {
        const queryParams = { friendCode };
        this.transitionTo('login', { queryParams });

        return;
      }

      const response = await fetch(url, {
        method: 'POST',
      });

      if (response.ok) {
        if (response.status === 204) {
          throw { errors: invalidCodeError };
        }

        const user = await response.json();
        this.transitionTo('friends.show', user.data.id);
      } else {
        const json = await response.json()

        throw json;
      }
    } catch(e) {
      let errors;
      if (e && e.errors) {
        errors = e.errors;
      } else {
        errors = unknownError;
      }

      return { friendCode, errors };
    }
  }
}
