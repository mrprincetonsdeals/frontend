import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class FriendsShowRoute extends Route {
  @service store;
  @service session;

  beforeModel(transition) {
    this.session.requireAuthentication(transition, () => {
      let loginController = this.controllerFor('login');
      loginController.set('previousTransition', transition);
      this.transitionTo('login');
    });
  }

  model(params) {
    return this.store.findRecord('friend', params.friend_id);
  }
}
