import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default class FriendsIndexRoute extends Route {
  queryParams = {
    page: {
      refreshModel: true,
    },
  };

  @service store;
  @service session;

  beforeModel(transition) {
    this.session.requireAuthentication(transition, () => {
      let loginController = this.controllerFor('login');
      loginController.set('previousTransition', transition);
      this.transitionTo('login');
    });
  }

  model(params) {
    if (params.page < 0) {
      this.transitionTo({ queryParams: { page: 0 } });
    }

    return hash({
      friends: this.store.query('friend', params),
      invites: this.store.query('invite', {}),
    });
  }

  setupController(controller, model) {
    super.setupController(controller, model);

    let invites = model.invites,
      friends = model.friends,
      length = friends.length,
      total = friends.meta.total,
      limit = friends.meta.limit;

    if (!length && total && limit) {
      this.transitionTo({ queryParams: { page: total / limit - 1 } });
    }

    controller.set('friends', friends);
    controller.set('total', total);
    controller.set('limit', limit);
    controller.set('invites', invites);
  }
}
