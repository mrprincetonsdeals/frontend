import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class CompaniesIndexRoute extends Route {
  queryParams = {
    page: {
      refreshModel: true,
    },
  };

  @service store;
  @service session;

  beforeModel(transition) {
    this.session.requireAuthentication(transition, () => {
      let loginController = this.controllerFor('login');
      loginController.set('previousTransition', transition);
      this.transitionTo('login');
    });
  }

  model(params) {
    if (params.page < 0) {
      this.transitionTo({ queryParams: { page: 0 } });
    }

    return this.store.query('company', params);
  }

  setupController(controller, model) {
    super.setupController(controller, model);

    let length = model.length,
      total = model.meta.total,
      limit = model.meta.limit;

    if (!length && total && limit) {
      this.transitionTo({ queryParams: { page: total / limit - 1 } });
    }

    controller.set('model', model);
    controller.set('total', total);
    controller.set('limit', limit);
  }
}
