import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class ProfileRoute extends Route {
  @service store;
  @service session;

  beforeModel(transition) {
    this.session.requireAuthentication(transition, () => {
      let loginController = this.controllerFor('login');
      loginController.set('previousTransition', transition);
      this.transitionTo('login');
    });
  }

  model() {
    return this.store.query('deal', {});
  }

  setupController(controller, model) {
    super.setupController(controller, model);

    controller.refreshRoute = () => this.refresh();
  }
}
