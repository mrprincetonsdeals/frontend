import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class LogoutRoute extends Route {
  @service session;
  @service store;

  beforeModel() {
    this.store.unloadAll();
    this.session.invalidate();

    this.replaceWith('home');
  }
}
